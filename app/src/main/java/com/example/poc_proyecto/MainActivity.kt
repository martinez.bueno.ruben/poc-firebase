package com.example.poc_proyecto

import android.content.Intent
import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key.Companion.Delete
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.example.poc_proyecto.ui.theme.PoCProyectoTheme
import com.google.firebase.FirebaseApp
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PoCProyectoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    layoutCrud()
                }
            }
        }
    }
}

@Preview
@Composable
fun layoutCrud(){
    val context = LocalContext.current
    val db = Firebase.firestore
    val firestore = FirebaseFirestore.getInstance()
/*
    val user = hashMapOf(
        "first" to "Ruben",
        "Last" to "Martinez",
        "Born" to "2003",
    )

db.collection("tareas")
    .add(user)
    .addOnSuccessListener { documentReference -> Log.d("Creacion","Se ha creado el usuario") }
    .addOnFailureListener{e-> Log.w("Error","No se ha creado el usuario")}
*/
    val user = rememberSaveable {mutableStateOf("")}
    var password = rememberSaveable {mutableStateOf("") }
    var estado = rememberSaveable {mutableStateOf("")}

    Box{
       Column(
           Modifier
               .fillMaxWidth()
               .fillMaxHeight())
       {
           Text("Enter your name:")
           OutlinedTextField(
               value = user.value,
               onValueChange = { newText ->
                   user.value = newText
               },
               label = { Text("Name") },
               keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text)
           )
           Text("Enter your password:")
           OutlinedTextField(
               value = password.value,
               onValueChange = { newText ->
                   password.value = newText
               },
               label = { Text("Password") },
               keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text)
           )



           Button(onClick = {
               val usuario = hashMapOf(
               "Usuario" to user.value,
               "Contrasenya" to password.value,
                   "Estado" to "estado pendiente"
                )
               firestore.collection("tareas")
               .add(usuario)
               .addOnSuccessListener { documentReference -> Log.d("Creacion","Se ha creado el usuario") }
               .addOnFailureListener{e-> Log.w("Error","No se ha creado el usuario")}
                user.value = ""
                password.value = ""
               Toast.makeText(context,"El usuario ha sido creado",Toast.LENGTH_LONG).show()
           }){
               Text(text = "Crear Usuario")

           }
           Button(onClick = {val intent = Intent(context, Listar::class.java)
               context.startActivity(intent)  }) {
               Text(text = "Listar Usuarios")
           }
            Button(onClick = { val intent = Intent(context, Deletes::class.java)
                context.startActivity(intent) }) {
                Text(text = "Eliminar Usuario")
            }
           Button(onClick = { val intent = Intent(context, UPDATE::class.java)
               context.startActivity(intent) }) {
               Text(text = "Actualizar")
           }
       }
    }
}



