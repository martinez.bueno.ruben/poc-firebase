package com.example.poc_proyecto

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.room.util.copy
import com.example.poc_proyecto.ui.theme.PoCProyectoTheme
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await

class Listar : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PoCProyectoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    listarUsersCrud()
                }
            }
        }
    }
}
@Preview
@Composable
fun listarUsersCrud(){
    val context = LocalContext.current
    val firestore = FirebaseFirestore.getInstance()
    val baseUsers = firestore.collection("tareas")
    var datosState by remember { mutableStateOf(mutableStateListOf<String>()) }
    val state = rememberLazyListState()


    LaunchedEffect(true){

        try {
            val documents: QuerySnapshot = baseUsers.get().await()
            for (document in documents.documents) {
                document.get("Usuario")?.let {
                    datosState.add(it.toString())
                }
            }
            Log.d("Documents", datosState.joinToString(","))
        }
        catch (e:Exception){
            Log.d("Error",e.toString())
        }
    }
    Box{
        Column(
            Modifier
                .fillMaxWidth()
                .fillMaxHeight())
        {
            LazyColumn(state = state) {
                items(datosState){ item ->
                    Text(text = item)
                }
                
            }
            Button(onClick = { val intent = Intent(context, Deletes::class.java)
                context.startActivity(intent) }) {
                Text(text = "Borrar")
            }
            Button(onClick = { val intent = Intent(context, MainActivity::class.java)
                context.startActivity(intent) }) {
                Text(text = "Volver")
            }

        }
    }
}
