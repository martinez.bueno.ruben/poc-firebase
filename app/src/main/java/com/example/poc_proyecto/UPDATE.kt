package com.example.poc_proyecto

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.example.poc_proyecto.ui.theme.PoCProyectoTheme
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class UPDATE : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PoCProyectoTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    layoutCrudUpdate()
                }
            }
        }
    }
}
@Preview
@Composable
fun layoutCrudUpdate(){
    val context = LocalContext.current
    val db = Firebase.firestore
    val firestore = FirebaseFirestore.getInstance()

    var user = remember { mutableStateOf("") }
    var password = remember { mutableStateOf("") }
    var estado = remember { mutableStateOf("") }
    var documentoReferencia: String? = null




    Box{
        Column(
            Modifier
                .fillMaxWidth()
                .fillMaxHeight())
        {
            Text("Enter your name for update Password")
            OutlinedTextField(
                value = user.value,
                onValueChange = { newText ->
                    user.value = newText
                },
                label = { Text("Name") },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text)
            )
            Text(text = "Enter new Password:")
            OutlinedTextField(
                value = password.value,
                onValueChange = { newText ->
                    password.value = newText
                },
                label = { Text("Password") },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text)
            )
            
            Button(onClick = {
                //db.collection("tareas").document("uYObYSULnyxM8konKCps").delete()

                val newPassword = mapOf(
                    "Contrasenya" to password.value,
                    "Estado" to "estado realizando"
                )

                var nombre = user
                db.collection("tareas")
                    .whereEqualTo("Usuario", nombre.value)
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            document.reference.update(newPassword)
                        }
                        user.value = ""
                        password.value = ""
                        Toast.makeText(context,"Se ha actualizado la contrasenya correctamente", Toast.LENGTH_LONG).show()
                    }
                    .addOnFailureListener { exception ->
                        println("Error actualizando: $exception")
                    }

            }){
                Text(text = "Update")

            }
            Button(onClick = { val intent = Intent(context, Deletes::class.java)
                context.startActivity(intent) }) {
                Text(text = "Borrar")
            }
            Button(onClick = { val intent = Intent(context, MainActivity::class.java)
                context.startActivity(intent) }) {
                Text(text = "Volver")
            }
            Button(onClick = {
                val finalizar = mapOf(
                    "Estado" to "estado Finalizado"
                )

                var nombre = user
                db.collection("tareas")
                    .whereEqualTo("Usuario", nombre.value)
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            document.reference.update(finalizar)
                        }
                        user.value = ""
                        password.value = ""
                        Toast.makeText(context,"Se ha finalizado el usuario", Toast.LENGTH_LONG).show()
                    }
                    .addOnFailureListener { exception ->
                        println("Error finalizando: $exception")
                    }


            }){
                Text(text = "Finalizar")
            }
        }
    }
}